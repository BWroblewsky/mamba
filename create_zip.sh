#!/usr/bin/env bash

name=bartlomiej_wroblewski

rm -rf $name*
mkdir $name

cp *.hs $name/.
cp Makefile $name/.
cp README.md $name/.
cp mamba.cf $name/.
cp -r good $name/.
cp -r bad $name/.

zip -r $name $name
