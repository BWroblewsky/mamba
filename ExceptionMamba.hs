module ExceptionMamba where

import System.Environment
import System.Exit hiding (die)
import System.IO

import Data.Map.Strict as Map

import PrintMamba
import AbsMamba

usage = putStrLn "Usage: ./Interpreter program_file.mba"
exit  = exitWith ExitSuccess
die   = exitWith (ExitFailure 1)


putStrErrLn :: String -> IO ()
putStrErrLn = hPutStrLn stderr

joinM :: Monad m => [m a] -> m [a]
joinM l = Prelude.foldr join (return []) l
  where join m ms = m >>= (\x -> ms >>= (\xs -> return (x:xs)))

data Exception
    = NotDeclared Ident
    | AlreadyDeclared Ident
    | NotApplicable Ident
    | IndexOutOfRange Ident Integer
    | NotIndexable Ident
    | Incompatible String Type Type
    | NotSupported String Type
    | ReturnInconsistent Type Type
    | Custom String
    | ExpectedType Type Expr Type
    | InconsistentReturn Type
    | DivisionByZero Expr
    | StrConvertion String

instance Show Exception where
  show (NotDeclared (Ident name))       = unwords ["Variable", name, "is not yet declared."]
  show (AlreadyDeclared (Ident name))   = unwords ["Variable", name, "was already declared on this level."]
  show (NotApplicable (Ident name))     = unwords ["Variable", name, "is not a function."]
  show (IndexOutOfRange (Ident name) p) = unwords ["Tuple", name, "does not have", show p, "position."]
  show (NotIndexable (Ident name))      = unwords ["Variable", name, "is not a tuple."]
  show (Incompatible op a b)            = unwords ["Cannot perform", op, "operation on", show a, "and", show b, "types."]
  show (NotSupported op a)              = unwords ["Operation", op, "does not support", show a, "type."]
  show (ReturnInconsistent a b)         = unwords ["Variable of type", show a, "cannot be returned, block returns", show b, "type."]
  show (Custom msg)                     = msg
  show (ExpectedType t expr t')         = unwords [printTree expr, "has type", show t' ++ ",", "but type", show t, "was expected."]
  show (InconsistentReturn t)           = unwords ["Function has to always return type", show t, ", but it does not."]
  show (DivisionByZero expr)            = unwords [printTree expr, "is equal to 0, cannot divide by it."]
  show (StrConvertion s)                = unwords [s, "cannot be converted to integer type."]
