## *Mamba* language
*Mamba* is an imperative language with built-in function and tuple types support.

Mamba requires mtl cabal package.

```
cabal update
cabal install mtl
```
 
### Points covered in assignment
6 points: all subpoints

10 points: all subpoints

14 points: subpoints 1-5, 6b, 6c

17 points: all subpoints

20 points: subpoints 8-10, 11d, 11f, 11g, 11h

24 points: all subpoints

## Example: Hello World
```
// Hello world
 
print_str("hello world");
```

## Example: Display even numbers to 10

```
// Display even and odd numbers to 10
 
is_even : Int -> Bool
is_even = function (x : Int) begin
  return x % 2 == 0;
end;
 
display : [Int, Int -> Bool] -> None
display = function (n : Int, f : Int -> Bool) begin
  for i = 0 to n do
    if f(i) then print_int(i); fi
  done
end;
 
// display even numbers, use defined function
display(10, is_even);
 
// display odd numbers, use anonymous function
display(10, function(x : Int) begin return x % 2 == 1; end);
 
// display odd numbers, use negated function
display(10, not is_even);
```

## Example: Factorial in two ways
 
```
main : None -> None
main = function () begin
  facti, factr : Int -> Int
 
  // iterative
  facti = function (n : Int) begin
    i_r = [1, 1] : [Int, Int]
    while i_r[0] < n + 1 do
      i_r = [i_r[0] + 1, i_r[0] * i_r[1]];
    done
    return i_r[1];
  end;
 
  //recursive
  factr = function (n : Int) begin
    if n < 2 then
      return 1;
    else
      return n * factr(n - 1);
    fi
  end;
 
  print_int(facti(7));
  print_int(factr(7));
end
 
main();
```

## Program structure

Program in *Mamba* is a block, and a block consists of statements.
Statements are executed from block's beginning to its end.

## Statements
#### Pass statement, Return statement
Like in Python.

```
pass;        \\ does nothing
return 1;    \\ returns 1
return;      \\ return None
```

Return in main program block works like exit, however it cannot return value

#### If / If-Else statement, While loop 
Like in Bash, but without brackets.
```
if x = 0 then a = 10; fi
if x = 0 then a = 10; else a = 1; fi
while x < 10 do decrease_x(); done
```

#### For loop 
Like in Pascal, both expressions used in it must evaluate to value of type `Int`, otherwise program 
is terminated before execution. Value after `to` defines value *greater* than last iteration. 
```
for i = 0 to n do print_int(i); done
```

Variable on which loop is working cannot be declared again inside loop block.
Also reassigning this value inside won't have effect on it's value in next iteration,
i.e.

```
for i = 0 to 5 do i = 10; print_int(i); done
```

will print `10` five times.

#### Expression statement 
Self explanatory, executes expression.

#### Variable declaration 
Declares a variable, defines its name and type. Variables can be declared anywhere 
in the block, however they cannot be referenced before declaration. Within the block the 
variables have to have distinct names. Variables cannot be used outside the block they're 
declared in and they cover variables with the same names defined in outer blocks. 
If the variable is declared but not initialized it is assigned default value depending 
on its type.
```
x : Int       // x is set to default Int value (0)
i = 10 : Int  // i is set to 10
```

#### Assignment 
Assigns value of expression to variable. Expression has to return the value of the
same type as type of variable, otherwise program is terminated before execution.
```
x : Int

x = 2 + 2;   // ok, type is correct
x = "abc";   // ERROR, expected Int type
```

## Types
There is no automatic type conversion. Using type in bad context results in termination 
of program before execution.

#### Int, Bool, String
Like in Java. Default values are 0 for `Int`, False for `Bool` and "" for `String`.
Mamba supports standard arithmetic syntax for `Int`, logical syntax for `Bool` and 
`String` concatenation with "++" symbol.

```
x = 2 + 2 * 2 : Int // 6 
b = not True or False : Bool // False
s = "Ala" ++ " ma" ++ " kota" : Str // "Ala ma kota"
```

#### Tuple
Like in Python. Type of a `Tuple` is a list of types of fixed length.
Tuples are immutable, their values can be changed only by reassignment.
Access to n-th element is possible using [] brackets notation (elements are numbered from 0).
Value in brackets must be a plane integer. 

`Tuple` can have another `Tuple` as its element.
Default value of tuple is based on default values of its inner types, i.e. default value of
`[Int, Bool, Bool]` is [0, False, False]

```
t = [0, False, "foo"] : [Int, Bool, Str]
t[0] = 1; // ERROR: tuples are immutable
t = [1, True, "bar"]; // Reassignment is OK
w = t[0] : Int // 1
```

#### Function
In mamba function is a type like any other. Type of function is described as pair of 
types - an argument type and a result type. Function must return the value of the type in its declaration.
All parameters are passed as value. Inside the block arguments behave as local variables.
Function definition consists of list of arguments and a block. 

```
f : [Int, Int] -> Int // Many arguments are represented as a Tuple
g : None -> Int // No arguments are represented as None
h : Int -> None // Function which does not return anything has None as return type
```

Functions can be defined inside another functions. Functions can be recursive, however to create 
indirect or direct recursion it is necessary to declare both functions before defining them (like in C).

```
g : Int -> Int
h : Int -> Int
 
g = function (x : Int) begin h(x); end;
h = function (x : Int) begin g(x); end; 
```

Function returning type X has to always return a value of type X.
It cannot returned value of another type, nor can it return nothing in any possible execution 
(not counting functions returning None).
In any of those cases error occurs during type-checking phase.

It's possible to perform operations on functions.

If two functions have the same type `T1 -> T2` it is possible to perform operation on them,
if this operation is enabled for type `T2`, and `T2` is the basic type. 
This operation returns a new function with the same type which executes both functions and 
returns the result of operation performed on their return values. Same thing is possible for one 
argument operations (`not`, `-`).

It is also possible to perform operation on function with type `T1 -> T2` and variable with basic type
`T2`. Result is a new function with same type `T1 -> T2`, which executes previous function and returns 
operation performed on it and a variable value.

Those operations use the value of variables, i.e. changing the component of operation later
does not change the result of operation (see example below).

```
f, g, h, q, w : Int -> Int
f = function (x : Int) begin return 2; end;
g = function (x : Int) begin return 3 * x; end;
h = f + g;
q = f - g;
w = f + 10;
 
print_int(h(5));  // prints 17 
print_int(q(3));  // prints -7
print_int(w(10)); // prints 12
 
// changing the f function doesn't change the functions w created with f
f = function (x : Int) begin return 5; end
print_int(f(0));  // prints 5
print_int(w(0));  // prints 12
```

Default value of undefined but declared function `T1 -> T2` is the constant function returning
the default value for type `T2`.

Because functions are treated like variables it is possible to create uncommon constructions.

```
result : None -> Int // function returns 0 by default
 
for i = 0 to 20 do
  result = result + i;
done
 
print_int(result()); // prints sum of integers from 0 to 20 (without 20)
 
f, g : None -> None
f = function() begin print_int(10); end;
g = function() begin 
  f = function() begin print_int(0); end; 
end;
 
f(); // prints 10
g();
f(); // prints 0
```

## Expressions

Logical expressions are NOT executed lazily (if first argument terminates expression, the second
is not executed). Same goes for logical expressions on variables and functions.

Functions definitions are treated as expressions, which enables use of anonymous functions.

## Built-in functions
Functions `print_*` and `read_*` perform corresponding i/o operations.

Functions `int_to_str` and `str_to_int` convert int/str to str/int. String can be converted to int
if it's prefix is a string representation of number, otherwise runtime exception occur.

Function `error` raises error. Errors are explicitly handled, i.e. running `error` prints
error message to stderr and terminates execution. Dynamic errors (for instance dividing by 0)
are handled in the same way with appropriate message.

```
print_int : Int -> None
print_str : Str -> None
 
read_int : None -> Int
read_str : None -> Str

int_to_str : Int -> Str
str_to_int : Str -> Int
 
error : Str -> None
```

## Full BNF description

```
-- programs ------------------------------------------------

entrypoints Program ;

Program.   Program ::= Block ;

-- statements ----------------------------------------------

Block.     Block ::= [Stmt] ;

separator  Stmt "" ;

Pass.      Stmt ::= "pass" ";" ;

Decl.      Stmt ::= [Item] ":" Type ;

NoInit.    Item ::= Ident ;

Init.      Item ::= Ident "=" Expr ;

separator nonempty Item "," ;

Ass.       Stmt ::= Ident "=" Expr ";" ;

Ret.       Stmt ::= "return" Expr ";" ;

NRet.      Stmt ::= "return" ";";

Cond.      Stmt ::= "if" Expr "then" Block "fi" ;

CondElse.  Stmt ::= "if" Expr "then" Block "else" Block "fi" ;

While.     Stmt ::= "while" Expr "do" Block "done" ;

For.       Stmt ::= "for" Item "to" Expr "do" Block "done" ;

SExp.      Stmt ::= Expr ";" ;

-- Types ---------------------------------------------------

Int.       Type1 ::= "Int" ;

Str.       Type1 ::= "Str" ;

Bool.      Type1 ::= "Bool" ;

None.      Type1 ::= "None" ;

Tuple.     Type1 ::= "[" [Type] "]" ;

Fun.       Type ::= Type1 "->" Type ;

coercions  Type 1 ;

separator  Type "," ;

-- Expressions ---------------------------------------------

Arg.       Arg ::= Ident ":" Type ;

separator  Arg "," ;

EVar.      Expr6 ::= Ident ;

ELitInt.   Expr6 ::= Integer ;

ELitTrue.  Expr6 ::= "True" ;

ELitFalse. Expr6 ::= "False" ;

EApp.      Expr6 ::= Ident "(" [Expr] ")" ;

EString.   Expr6 ::= String ;

ETuple.    Expr6 ::= "[" [Expr] "]" ;

EFun.      Expr6 ::= "function" "(" [Arg] ")" "begin" Block "end" ;

EElem.     Expr6 ::= Ident "[" Integer "]" ;

Neg.       Expr5 ::= "-" Expr6 ;

Not.       Expr5 ::= "not" Expr6 ;

EMul.      Expr4 ::= Expr4 MulOp Expr5 ;

EAdd.      Expr3 ::= Expr3 AddOp Expr4 ;

ERel.      Expr2 ::= Expr2 RelOp Expr3 ;

EAnd.      Expr1 ::= Expr2 "and" Expr1 ;

EOr.       Expr ::= Expr1 "or" Expr ;

coercions  Expr 6 ;

separator  Expr "," ;

-- operators -----------------------------------------------

Plus.      AddOp ::= "+" ;

Concat.    AddOp ::= "++" ;

Minus.     AddOp ::= "-" ;

Times.     MulOp ::= "*" ;

Div.       MulOp ::= "/" ;

Mod.       MulOp ::= "%" ;

LTH.       RelOp ::= "<" ;

LE.        RelOp ::= "<=" ;

GTH.       RelOp ::= ">" ;

GE.        RelOp ::= ">=" ;

EQU.       RelOp ::= "==" ;

NE.        RelOp ::= "!=" ;

-- comments ------------------------------------------------

comment    "//" ;

comment    "/*" "*/" ;
```
