module TypingMamba where

import Control.Monad.State
import Control.Monad.Reader
import Data.Map.Strict as Map hiding (foldl, map)
import System.IO

import AbsMamba
import ExceptionMamba

type TEnv = (Map Ident Type, [Ident]) -- map of ident to type, list of idents declared on this level
type TState = (Maybe Type, Maybe Type) -- type returned by this function, type returned by this block
type TypingMonad a = ReaderT TEnv (StateT TState IO) a

throwError :: Exception -> TypingMonad a
throwError err = lift $ lift $ putStrErrLn (show err) >> die

initTEnv :: TEnv
initTEnv = ((insert (Ident "print_int")   (Fun Int None) $
             insert (Ident "print_str")   (Fun Str None) $
             insert (Ident "read_int")    (Fun None Int) $
             insert (Ident "read_str")    (Fun None Str) $
             insert (Ident "int_to_str")  (Fun Int Str)  $
             insert (Ident "str_to_int")  (Fun Str Int)  $
             insert (Ident "error")       (Fun Str None) $ empty), [])

initTState :: TState
initTState = (Just None, Nothing)

getReturnType :: TypingMonad Type
getReturnType = get >>= \(maybe_t, _) -> case maybe_t of {Just t -> return t; Nothing -> return None}

getBlockReturnType :: TypingMonad (Maybe Type)
getBlockReturnType = get >>= \(_, maybe_t) -> return maybe_t

addReturnType :: Type -> TypingMonad ()
addReturnType t = get >>= \(maybe_t, _) -> case maybe_t of
  Nothing -> put $ (Just t, Just t)
  Just t' -> if t === t' then put $ (Just t, Just t)
                         else throwError $ ReturnInconsistent (simplifyType t) (simplifyType t')

preserveReturnType :: TypingMonad a -> TypingMonad Type
preserveReturnType m = get >>= (\r -> put (Nothing, Nothing) >> m >> getReturnType >>= (\r' -> put r >> return r'))

preserveBlockReturnType :: TypingMonad a -> TypingMonad (Maybe Type)
preserveBlockReturnType m = get >>= (\(t, r) -> put (t, Nothing) >> m >> get >>= (\(t', r') -> put(t', r) >> return r'))

getType :: Ident -> TypingMonad Type
getType ident = ask >>= \(env, ids) -> case Map.lookup ident env of {Just t  -> return t;
                                                                     Nothing -> throwError $ NotDeclared ident}

simplifyType :: Type -> Type
simplifyType (Tuple [])   = None
simplifyType (Tuple [t])  = simplifyType t
simplifyType (Tuple ts)   = Tuple (map simplifyType ts)
simplifyType (Fun t1 t2)  = Fun (simplifyType t1) (simplifyType t2)
simplifyType t            = t

(===) :: Type -> Type -> Bool
t1 === t2 = simplifyType t1 == simplifyType t2

joinTypes :: Exception -> Type -> Type -> TypingMonad Type
joinTypes err t1 t2 = case (simplifyType t1, simplifyType t2) of
  (Fun _ _, Fun _ _) -> if t1 == t2 then return t1 else throwError err
  (Fun _ r1, _)      -> if r1 == t2 then return t1 else throwError err
  (_, Fun _ r2)      -> if t1 == r2 then return t2 else throwError err
  (_, _)             -> if t1 == t2 then return t1 else throwError err

expectedExpr :: Type -> Expr -> TypingMonad ()
expectedExpr t expr = typingExpr expr
  >>= \t' -> if t === t' then return () else throwError $ ExpectedType (simplifyType t) expr (simplifyType t')

typingExpr :: Expr -> TypingMonad Type
typingExpr (ELitInt _)  = return Int
typingExpr ELitTrue     = return Bool
typingExpr ELitFalse    = return Bool
typingExpr (EString _)  = return Str
typingExpr (EVar ident) = getType ident
typingExpr (ETuple es)  = joinM (map typingExpr es) >>= \ts -> return $ Tuple ts

typingExpr (EApp ident exprs) = getType ident >>= \f_type -> case f_type of
    Fun a b -> expectedExpr a (ETuple exprs) >> return b
    _       -> throwError $ NotApplicable ident

typingExpr (EElem ident pos) = getType ident >>= \t -> case t of
    Tuple ts -> if fromIntegral pos < length ts then return (ts!!(fromIntegral pos))
                                                else throwError (IndexOutOfRange ident pos)
    _        -> throwError $ NotIndexable ident

typingExpr (Neg expr) = typingOper1 "Neg" Int expr
typingExpr (Not expr) = typingOper1 "Not" Bool expr
typingExpr (EMul expr1 op expr2)      = typingOper2 (show op) Int expr1 expr2
typingExpr (EAdd expr1 Minus expr2)   = typingOper2 (show Minus) Int expr1 expr2
typingExpr (EAdd expr1 Plus expr2)    = typingOper2 (show Plus) Int expr1 expr2
typingExpr (EAdd expr1 Concat expr2)  = typingOper2 (show Concat) Str expr1 expr2
typingExpr (ERel expr1 op expr2)      = typingOper2 (show op) Int expr1 expr2
                                      >>= (\t -> case t of {Int -> return Bool; (Fun a Int) -> return $ Fun a Bool})
typingExpr (EAnd expr1 expr2)         = typingOper2 "And" Bool expr1 expr2
typingExpr (EOr expr1 expr2)          = typingOper2 "Or" Bool expr1 expr2

typingExpr (EFun args (Block stmts)) = do
  let a_type = Tuple $ map (\(Arg _ t) -> t) args
  let decls = map (\(Arg ident t) -> Decl [NoInit ident] t) args
  r_type <- preserveReturnType $ typingBlock (Block (decls ++ stmts)) >> get >>= (\r -> case r of
    (Just None, Nothing) -> return ()
    (Nothing, _)         -> return ()
    (Just t1, t2)        -> if (Just t1) == t2 then return () else throwError $ InconsistentReturn t1)
  return $ Fun (simplifyType a_type) (simplifyType r_type)

typingOper1 :: String -> Type -> Expr -> TypingMonad Type
typingOper1 op expect expr = typingExpr expr >>= \t -> joinTypes (NotSupported op t) expect t

typingOper2 :: String -> Type -> Expr -> Expr -> TypingMonad Type
typingOper2 op expect expr1 expr2 = do
  t1 <- typingExpr expr1 >>= \t -> joinTypes (NotSupported op t) expect t
  t2 <- typingExpr expr2 >>= \t -> joinTypes (NotSupported op t) expect t
  joinTypes (Incompatible op t1 t2) t1 t2

typingItem :: Type -> Item -> TEnv -> TypingMonad TEnv
typingItem t (Init ident expr) (env, ids) = expectedExpr t expr >> typingItem t (NoInit ident) (env, ids)
typingItem t (NoInit ident) (env, ids) = if elem ident ids then throwError $ AlreadyDeclared ident
                                                           else return (insert ident t env, ident:ids)

typingStmts :: [Stmt] -> TypingMonad ()
typingStmts [] = return ()

typingStmts (stmt:stmts) = case stmt of
  Pass  -> typingStmts stmts
  (Decl items t) -> foldl (>>=) ask (map (typingItem t) items) >>= \env -> local (const env) (typingStmts stmts)
  (Ass ident expr) -> getType ident >>= \t -> expectedExpr t expr >> typingStmts stmts

  NRet -> addReturnType None >> typingStmts stmts
  (Ret expr) -> typingExpr expr >>= addReturnType >> typingStmts stmts

  (Cond expr b1)  -> expectedExpr Bool expr >> (preserveBlockReturnType $ typingBlock b1) >> typingStmts stmts
  (CondElse expr b1 b2) -> do
    expectedExpr Bool expr
    t1 <- preserveBlockReturnType $ typingBlock b1
    t2 <- preserveBlockReturnType $ typingBlock b2
    t' <- getBlockReturnType
    if t1 == t2 then case t' of  {Just t -> return (); Nothing -> modify $ \(t, _) -> (t, t1)} else return ()
    typingStmts stmts

  (While expr b1) -> expectedExpr Bool expr >> (preserveBlockReturnType $ typingBlock b1) >> typingStmts stmts
  (For item expr (Block stmts')) -> expectedExpr Int expr
    >> (preserveBlockReturnType $ typingBlock (Block ((Decl [item] Int):stmts'))) >> typingStmts stmts

  (SExp expr)     -> typingExpr expr >> typingStmts stmts

typingBlock :: Block -> TypingMonad ()
typingBlock (Block stmts) = local (\(env, _) -> (env, [])) (typingStmts stmts)

typingProgram :: Program -> IO ()
typingProgram (Program block) = evalStateT (runReaderT (typingBlock block) initTEnv) initTState
                              >> return ()
