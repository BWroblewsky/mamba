module ExecuteMamba where

import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.Reader
import Data.Maybe
import Data.Map.Strict as Map
import System.IO

import AbsMamba
import ExceptionMamba

data Value = NVal | S String | B Bool | I Integer | T [Value] | F (Value -> ExecutionMonad Value)

type EEnv = Map Ident Integer
type EState = Map Integer Value
type ExecutionMonad a = ExceptT Value (ReaderT EEnv (StateT EState IO)) a

throwRuntimeError :: Exception -> ExecutionMonad a
throwRuntimeError err = lift $ lift $ lift $ putStrErrLn (show err) >> die

initEEnv :: EEnv
initEEnv = insert (Ident "print_int")   0 $
           insert (Ident "print_str")   1 $
           insert (Ident "read_int")    2 $
           insert (Ident "read_str")    3 $
           insert (Ident "int_to_str")  4 $
           insert (Ident "str_to_int")  5 $
           insert (Ident "error")       6 $ empty

initEState :: EState
initEState = insert 0 (F (\(I i) -> lift $ lift $ lift $ putStrLn (show i) >> return NVal)) $
             insert 1 (F (\(S s) -> lift $ lift $ lift $ putStrLn s >> return NVal))        $
             insert 2 (F (\NVal -> lift $ lift $ lift $ readLn >>= \i -> return (I i)))     $
             insert 3 (F (\NVal -> lift $ lift $ lift $ getLine >>= \s -> return (S s)))    $
             insert 4 (F (\(I i) -> return (S $ show i)))                                   $
             insert 5 (F (\(S s) -> case (fmap fst . listToMaybe . reads) s of
                                      Nothing -> throwRuntimeError $ StrConvertion s
                                      Just i  -> return (I i)))                             $
             insert 6 (F (\(S s) -> lift $ lift $ lift $ putStrErrLn s >> die))             $ empty


newValue :: Ident -> Value -> EEnv -> ExecutionMonad EEnv
newValue ident v env = do
  state <- get
  let loc = toInteger $ size state
  put $ insert loc v state
  return $ insert ident loc env

setValue :: Ident -> Value -> ExecutionMonad ()
setValue ident v = do
  state <- get
  env <- ask
  let (Just loc) = Map.lookup ident env
  put $ insert loc v state

getValue :: Ident -> ExecutionMonad Value
getValue ident = do
  env <- ask
  state <- get
  let (Just value) = Map.lookup ident env >>= (\l -> Map.lookup l state >>= (\v -> return v))
  return value

defaultValue :: Type -> ExecutionMonad Value
defaultValue None = return NVal
defaultValue Int  = return $ I 0
defaultValue Str  = return $ S ""
defaultValue Bool = return $ B False
defaultValue (Tuple ts) = joinM (fmap defaultValue ts) >>= \vs -> return $ T vs
defaultValue (Fun t t') = return (F (\_ -> defaultValue t'))

simplifyValue :: Value -> ExecutionMonad Value
simplifyValue (T [])  = return NVal
simplifyValue (T [v]) = return v
simplifyValue v       = return v

tupleValue :: Value -> ExecutionMonad Value
tupleValue NVal   = return $ T []
tupleValue (T vs) = return $ T vs
tupleValue v      = return $ T [v]

executeExpr :: Expr -> ExecutionMonad Value
executeExpr (ELitInt i)  = return $ I i
executeExpr ELitTrue     = return $ B True
executeExpr ELitFalse    = return $ B False
executeExpr (EString s)  = return $ S s

executeExpr (EVar ident) = getValue ident
executeExpr (ETuple es)  = joinM (fmap executeExpr es) >>= \vs -> return $ T vs
executeExpr (EElem ident pos) = getValue ident >>= \(T vs) -> return $ vs !! (fromIntegral pos)

executeExpr (EApp ident exprs) = do
  x <- getValue ident
  (F fun) <- getValue ident
  args <- joinM $ fmap executeExpr exprs
  simpl_args <- simplifyValue $ T args
  fun simpl_args

executeExpr (Neg expr) = executeOper1 expr $ \(I i) -> (return $ I (-i))
executeExpr (Not expr) = executeOper1 expr $ \(B b) -> (return $ B $ not b)

executeExpr (EMul expr1 Times expr2) = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ I $ i1 * i2)
executeExpr (EMul expr1 Div expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (
    if i2 == 0 then throwRuntimeError (DivisionByZero expr2) else return $ I $ i1 `div` i2)
executeExpr (EMul expr1 Mod expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ I $ i1 `mod` i2)

executeExpr (EAdd expr1 Minus expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ I $ i1 - i2)
executeExpr (EAdd expr1 Plus expr2)    = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ I $ i1 + i2)
executeExpr (EAdd expr1 Concat expr2)  = executeOper2 expr1 expr2 $ \(S s1) -> \(S s2) -> (return $ S $ s1 ++ s2)

executeExpr (ERel expr1 LTH expr2)  = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 < i2)
executeExpr (ERel expr1 LE expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 <= i2)
executeExpr (ERel expr1 GTH expr2)  = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 > i2)
executeExpr (ERel expr1 GE expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 >= i2)
executeExpr (ERel expr1 EQU expr2)  = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 == i2)
executeExpr (ERel expr1 NE expr2)   = executeOper2 expr1 expr2 $ \(I i1) -> \(I i2) -> (return $ B $ i1 /= i2)

executeExpr (EAnd expr1 expr2)  = executeOper2 expr1 expr2 $ \(B b1) -> \(B b2) -> (return $ B $ b1 && b2)
executeExpr (EOr expr1 expr2)   = executeOper2 expr1 expr2 $ \(B b1) -> \(B b2) -> (return $ B $ b1 || b2)

executeExpr (EFun args (Block stmts)) = do
  env <- ask
  return $ F (\v -> do
    (T vs) <- tupleValue v
    env' <- Prelude.foldl (>>=) (return env) $ fmap (\(Arg ident _, v) -> newValue ident v) (zip args vs)
    catchE (local (const env') (executeStmts stmts)) return)

executeOper1 :: Expr -> (Value -> ExecutionMonad Value) -> ExecutionMonad Value
executeOper1 expr op = do
  v <- executeExpr expr
  case v of
    F f -> return $ F (\v -> f v >>= op)
    _   -> op v

executeOper2 :: Expr -> Expr -> (Value -> Value -> ExecutionMonad Value) -> ExecutionMonad Value
executeOper2 expr1 expr2 op = do
  v1 <- executeExpr expr1
  v2 <- executeExpr expr2
  case (v1, v2) of
    (F f1, F f2) -> return $ F (\v -> f1 v >>= (\v1 -> f2 v >>= (\v2 -> op v1 v2)))
    (F f1, _)    -> return $ F (\v -> f1 v >>= (\v1 -> op v1 v2))
    (_, F f2)    -> return $ F (\v -> f2 v >>= (\v2 -> op v1 v2))
    _            -> op v1 v2

executeItem :: Type -> Item -> EEnv -> ExecutionMonad EEnv
executeItem t (NoInit ident) env = defaultValue t >>= (\v -> newValue ident v env)
executeItem _ (Init ident expr) env = executeExpr expr >>= (\v -> newValue ident v env)

executeStmts :: [Stmt] -> ExecutionMonad Value
executeStmts [] = return NVal
executeStmts (stmt:stmts) = case stmt of
  Pass  -> executeStmts stmts

  (Decl items t)    -> do
    env' <- Prelude.foldl (>>=) ask (fmap (executeItem t) items)
    local (const env') (executeStmts stmts)

  (Ass ident expr)  -> executeExpr expr >>= (setValue ident) >> executeStmts stmts
  (SExp expr) -> executeExpr expr >> executeStmts stmts

  (NRet)      -> throwE NVal
  (Ret expr)  -> executeExpr expr >>= throwE

  (Cond expr b1)  -> do
    (B b) <- executeExpr expr
    if b then executeBlock b1 else return NVal
    executeStmts stmts

  (CondElse expr b1 b2) -> do
    (B b) <- executeExpr expr
    if b then executeBlock b1 else executeBlock b2
    executeStmts stmts

  (While expr b1) -> do
    (B b) <- executeExpr expr
    if b then executeBlock b1 >> executeStmts (stmt:stmts) else executeStmts stmts

  (For item expr b1) -> do
    (I bottom) <- case item of {(NoInit _) -> defaultValue Int; (Init _ e) -> executeExpr e}
    ident      <- case item of {(NoInit ident) -> return ident; (Init ident e) -> return ident}

    fix (\f i ident -> do
      env' <- ask >>= newValue ident (I i)
      local (const env') (executeExpr expr >>= \(I limit) -> if i < limit then executeBlock b1 >> (f (i+1) ident)
                                                                          else executeStmts stmts)) bottom ident

executeBlock :: Block -> ExecutionMonad Value
executeBlock (Block stmts) = executeStmts stmts

executeProgram :: Program -> IO ()
executeProgram (Program block) = evalStateT (runReaderT (runExceptT (executeBlock block)) initEEnv) initEState
                              >> return ()
