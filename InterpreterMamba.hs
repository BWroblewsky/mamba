import System.Environment
import System.Exit hiding (die)
import System.IO

import Data.Map.Strict as Map

import LexMamba
import ParMamba
import SkelMamba
import PrintMamba
import AbsMamba

import ErrM

import TypingMamba
import ExecuteMamba
import ExceptionMamba

main :: IO ()
main = getArgs >>= parseArgs

parseArgs :: [String] -> IO ()
parseArgs [filename]  = parseProgram filename >>= runProgram
parseArgs _           = usage >> exit

parseProgram :: String -> IO Program
parseProgram filename = do
  file_content <- readFile filename
  let program_tree = pProgram $ myLexer file_content
  case program_tree of
    Bad err -> do putStrLn "Bad syntax: " >> putStrLn err >> die
    Ok tree -> do return tree

runProgram :: Program -> IO()
runProgram tree = (typingProgram tree) >> (executeProgram tree)
